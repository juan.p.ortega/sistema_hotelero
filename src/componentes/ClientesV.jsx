import React from "react";
import Bootstrap from "../css/Bootstrap.css";
import SelectorFecha from "./SelectorFecha";
import swal from 'sweetalert';
import { useEffect, useState } from 'react';
import img1 from "../images/imagenesc.png";
import { Clientes } from "../hooks/ConexionSw";
import {useNavigate,Link, Navigate } from 'react-router-dom';
import VisualizarReservaciones from "./VisualizarReservaciones";

const ClientesV = () => {

    const [info, setInfo] = useState(undefined);
    const [ex, setEx] = useState(undefined);
    const [llamada, setllamada] = useState(false);
    const navigate = useNavigate()

    const mensaje = (texto) => swal(
        {
            title: "Error",
            text: texto,
            icon: "error",
            button: "Aceptar",
            timer: 2000
        }
    );

    const manejarClickCelda = (dato) => {
        console.log('Dato de la celda:', dato);
        //navigate("/reservaciones/guardar/"+dato);
        setEx(dato)
        return dato;
        // Aquí puedes hacer lo que necesites con el dato
    };



    if (!llamada) {
        const datos = Clientes().then((data) => {
            //console.log(data.fila);
            setllamada(true);
            setInfo(data);
        }, (error) => {
            //console.log(error);
            mensaje(error.mensaje);
        });
    }

    return (
        <div className="container" style={{ height: "1460px", width: "2600px" }}>
            <div className="row d-flex justify-content-center">
                <h1 className="font-weight-bold mx-3 "> <b>CLIENTES</b> </h1>
            </div>
            <div className="row d-flex justify-content-center">
                <img style={{ height: "200px", width: "200px" }} src={img1} className="card-img-top" alt="ImagenReservacion" />
            </div>
            <div className="container mt-5  ">
                <table className="table table-hover table-dark tableFixHead ">
                    <thead className="border-light">
                        <tr>
                            <th scope="col">
                                <strong> Numero </strong>
                            </th>
                            <th scope="col">
                                <strong> Nombre </strong>
                            </th>
                            <th scope="col">
                                <strong> Apellidos </strong>
                            </th>
                            <th scope="col">
                                <strong> Direccion </strong>
                            </th>
                            <th scope="col">
                                <strong> Identificacion </strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {info && info.data && info.data.map((element, key) => {
                            return <tr key={key}>
                                <td onClick={() => manejarClickCelda(key + 1)}>{(key) + 1}</td>
                                <td onClick={() => manejarClickCelda(element.external)}> {element.nombres}</td>
                                <td onClick={() => manejarClickCelda(element.external)}> {element.apellidos}</td>
                                <td onClick={() => manejarClickCelda(element.external)}> {element.direccion}</td>
                                <td onClick={() => manejarClickCelda(element.external)}> {element.identificacion}</td>
                                <td> <Link to={"/VisualizarReservaciones/"+element.external+"/0"} className='btn btn-dark'  onClick={<Navigate to={'/VisualizarReservaciones/'+element.external+"/0"}/>}> Seleccionar </Link> </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default ClientesV;