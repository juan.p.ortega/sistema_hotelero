import React from 'react';
import '../css/Bootstrap.css'
import { useForm } from 'react-hook-form';
import cardenal from "../images/cardenal.png";
import { Link, useNavigate } from 'react-router-dom';
import swal from 'sweetalert';
import { IngresarSistema } from '../hooks/ConexionSw';
import { Session } from '../utilidades/UseSession';

const RegistrarSistemaView = () => {
    const navegacion = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const mensaje = (texto) => swal (
        {
          title: "Error",
          text: texto,
          icon: "error",
          button: "Aceptar",
          timer: 2000
        }
      );

      const mensajeOk = (texto) => swal (
        {
          title: "Ingresado Correctamente",
          text: texto,
          icon: "success",
          button: "Aceptar",
          timer: 2000
        }
      );
  
     /* const llamar = (datos) => {
            execute(datos);
      };*/
  
      const onSubmit = (data) => {
          var datos = {'correo': data.correo, 'clave': data.clave};
           IngresarSistema(datos).then((info) => {
              if(info && info.data.token){
                console.log(info.data.token);
                  Session(info.data.token);
                  mensajeOk("Bienvenido")
                  navegacion('/inicio');
              }else{
                  mensaje(info.msg);
              }
            });
          };
    return (
        <div className='row justify-content-center' style={{ height: "1460px", width: "2600px" }}>
            <div className='row m-2 col-8 border border-dark'>
                <div align='left' className='mt-4'>
                    <img src={cardenal} className="img-fluid" width="100" height="100" alt="Logo" style={{ border: "solid" }} />
                </div>
                <form className='align-items-center justify-content-center h-50 col-4 mt-5 mx-5 ' onSubmit={handleSubmit(onSubmit)}>
                    <img src='https://cdn-icons-png.flaticon.com/512/295/295128.png' className="img-fluid" alt='usuario' width="200" height="200" />
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label"> <b>Email</b> </label>
                        <input type="email"
                            {...register('correo', { required: true, pattern: /\S+@\S+\.\S+/ })}
                            placeholder='Correo Electronico' className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        {errors.correo && errors.correo.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere su correo</div>}
                        {errors.correo && errors.correo.type === 'pattern' && <div className='alert alert-danger fade show' role='alert'>Ingrese un correo valido</div>}
                        <div id="emailHelp" className="form-text"></div>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputPassword1" className="form-label"><b>Password</b> </label>
                        <input {...register('clave', { required: true })} type="password" placeholder='Clave' className="form-control" id="exampleInputPassword1" />
                        {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere su clave</div>}
                    </div>
                    <div className="mb-3 form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                        <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" className="btn btn-dark">
                        <b>Login</b>
                        <img src="https://cdn-icons-png.flaticon.com/128/1176/1176390.png" width={"60"} height={"65"}></img> <b style={{ fontSize: "x-large" }}> </b>
                    </button>

                    <div className='mt-5'>
                        <p style={{ fontSize: "x-large" }}> <b> Si no tiene cuenta registrese a continuación </b> </p>
                        <button type="submit" className="btn btn-dark">
                            <img src="https://cdn-icons-png.flaticon.com/128/863/863823.png" width={"60"} height={"65"}></img>
                            <Link className="btn btn-dark" to="/CrearCuenta"> <h4> <b>Crear Cuenta</b> </h4></Link>
                        </button>
                    </div>

                </form>
                <div className='m-5 umn justify-content-center'>
                    <img src="https://media-cdn.tripadvisor.com/media/photo-s/1c/4a/db/c7/nuestro-hotel-a-tu-servicio.jpg" className="img-fluid" alt="Publicidad" width="500" height="500" style={{ border: "solid" }} />
                </div>
            </div>
        </div>
    );
};

export default RegistrarSistemaView;