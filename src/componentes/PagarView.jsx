import React from 'react';
import '../css/Bootstrap.css'
import cardenal from "../images/cardenal.png";

const PagarView = () => {
    return (
        
        <div className="row justify-content-center" style={{margin:"0px auto" }}>
            <div className='justify-content-center m-5 col-6 border border-dark'>
            <h2 className="container" style={{textAlign: "center"}}> <b> PAGAR</b></h2>
                <div align='left'>
                    <img src={cardenal} className="img-fluid" width="150px" height="150px" alt="Logo" />
                </div>
                <div className='d-flex justify-content-center '>
                    <div className='d-flex flex-column'>
                        <img src='https://maleconinn.com/wp-content/uploads/2021/03/jacuzzi-a.jpg' width="200" height="200" alt='cuarto' />
                    </div>
                    <div className='m-3 d-flex flex-column'>
                        <input type='text' placeholder='SUIT' />
                        <p>1 Habitacion Doble</p>
                        <p>1 Baño con tina</p>
                        <input type='text' placeholder='nro de dias de hospedaje' />
                        <input type='text' placeholder='Recargos adicionales' />
                    </div>

                </div>

                <div>
                    <label htmlFor="guestname" className="mt-4 field-label">TOTAL:  </label>
                    <input type='text' placeholder='0.00' />
                </div>

                <div>
                    <button type="submit" className="mt-3 btn btn-primary">Pagar</button>
                </div>

                <div align='center'>
                    <img src='https://www.arpatel.com.ec/wp-content/uploads/2017/12/tarjetas-credito-logos-300x91.png' className="img-fluid align-items-center justify-content-center h-45" with='25' height="25" alt="tarjetas" />
                </div>
            </div>
        </div>
    );
};

export default PagarView;