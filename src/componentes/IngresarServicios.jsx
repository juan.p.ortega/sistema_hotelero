import React from "react";
import "../css/Bootstrap.css";
import { Session } from '../utilidades/UseSession';
import { Servicios } from '../hooks/ConexionSw';
import {useNavigate,Link, Navigate,useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { useEffect, useState } from 'react';
import { IngresarServicios } from '../hooks/ConexionSw';
import { useForm } from 'react-hook-form';
//import '../css/fondo.css'

const mensaje = (texto) => swal(
  {
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 2000
  }
);

const mensajeOk = (texto) => swal(
  {
    title: "Ingresado Correctamente",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 2000
  }
);

const VerServicios = () => {
  //const navegacion = useNavigate();

  const { external } = useParams();
  const [info, setInfo] = useState(undefined);
  const [llamada, setllamada] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();

  if (!llamada) {
    const datos = Servicios().then((data) => {
      //console.log(data.fila);
      
      setllamada(true);
      setInfo(data);
      //console.log(data);
    }, (error) => {
      //console.log(error);
      mensaje(error.mensaje);
    });
  }

  const manejarClickCelda = (dato) => {
    console.log('Dato de la celda:', dato);
    // Aquí puedes hacer lo que necesites con el dato
  };

  const onSubmit = (data) => {
    var datos = { 'nombreServicio': data.nombreServicio, 'precio': data.precio };
    const val = IngresarServicios(datos).then((info) => {
      if (info) {
        console.log("---",info.data);
        //console.log(info.datos);
        mensajeOk("Se han ingresado los datos");
      } else {
        mensaje(info.message);
        console.log("NO Se han ingresado los datos");
      }
    });
  };

  return (
    <div className="container" style={{ height: "1460px", width: "2600px" }}>
      <div className="row d-flex justify-content-center">
        <h1 className="font-weight-bold mx-3 "> INGRESAR SERVICIOS </h1>
        <img
          src="https://cdn-icons-png.flaticon.com/512/686/686379.png"
          width={"50"}
          height={"65"}
        ></img>
      </div>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row d-flex justify-content-center">
          <div className="mt-4 mx-5">
            <label > <b>Precio</b> </label>
            <input className="text-center mx-1"
              placeholder="precio"
              aria-describedby="search-addon"
              style={{ width: "200px", height: "40px" }} type="number" step="any"{...register('precio', { required: true })} />
              {errors.precio && errors.precio.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere el precio del Servicio</div>}
          </div>
          <div className="mx-6 mt-4">
            <label > <b> Nombre Servicio </b></label>
            <input className="text-center mx-1"
              placeholder="Nombre Servicio"
              aria-describedby="search-addon"
              style={{ width: "200px", height: "40px" }} type="text" {...register('nombreServicio', { required: true })} />
              {errors.nombreServicio && errors.nombreServicio.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere el nombreServicio del Servicio</div>}
          </div>
          <div className="mx-5 mt-4">
            <button type="submit" className="btn btn-dark"> <b>Ingresar Servicio</b></button>
          </div>
        </div>
      </form>

      <div className="container mt-5  ">
        <table className="table table-hover table-dark tableFixHead ">
          <thead className="border-light">
            <tr>
              <th scope="col">
                <strong> Numero</strong>
              </th>
              <th scope="col">
                <strong> Tipo de Servicio</strong>
              </th>
              <th scope="col">
                <strong> Precio </strong>
              </th>
            </tr>
          </thead>
          <tbody>
            {info && info.data && info.data.map((element, key) => {
              console.log(external);
              return <tr key={key}>
                <td onClick={() => manejarClickCelda(key+1)}>{(key) + 1}</td>
                <td onClick={() => manejarClickCelda(element.external)}> {element.Nombre_Servicio}</td>
                <td onClick={() => manejarClickCelda(element.external)}> {element.precio}</td>
                <td> <Link to={"/VisualizarReservaciones/"+external+"/"+element.external} className='btn btn-dark'  onClick={<Navigate to={"/VisualizarReservaciones/"+external+"/"+element.external}/>}> Seleccionar </Link> </td>
              </tr>
            })}
          </tbody>
        </table>
      </div>
      <dir></dir>
    </div>
  );
};

export default VerServicios;
