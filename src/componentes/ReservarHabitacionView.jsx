import React from 'react';

const ReservarHabitacionView = () => {
    return (
        <div className="row justify-content-center" style={{height: "1460px" ,width: "2600px"}}>
            <div className='justify-content-center align-items-center m-5 col-6 border border-dark'>
              <h2 className="container" style={{textAlign: "center"}}> <b> RESERVAR HABITACIONES</b></h2>
                <div align='left'>
                    <img src='https://cdn.iconscout.com/icon/premium/png-256-thumb/hotel-signboard-44-896955.png' width="100" height="100" className="img-fluid" alt="Logo" />
                </div>
                <div>
                    <input type='text' placeholder='Buscar' />
                    <button type="submit" className="m-4 btn btn-primary">Buscar</button>
                </div>
                <div className='d-flex justify-content-center align-items-center'>

                    <div className='d-flex justify-content-center align-items-center m-3'>
                        <input type='text' placeholder='check-in' />
                        <input type="date" />
                        <input type='text' placeholder='check-out' />
                        <input type="date" />
                    </div>
                    <div className='d-flex justify-content-center align-items-center'>
                        <input type='text' placeholder='Nro. Huespedes' />
                    </div>
                </div>

                <div className='d-flex justify-content-center align-items-center ' >
                    <div className='d-flex flex-column'>
                        <img src='https://maleconinn.com/wp-content/uploads/2021/03/jacuzzi-a.jpg' width="200" height="200" alt='cuarto' />
                        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlJ_u9n6VplSDsVWGQJdCRb_m6qsas7e658A&usqp=CAU' width="200" height="200" className='mt-4' alt='cuarto' />
                    </div>
                    <div className='m-3 d-flex flex-column' >
                        <input type='text' placeholder='SUIT' />
                        <p>1 Habitacion Doble</p>
                        <p>1 Baño con tina</p>
                        <input type='text'
                            placeholder='GRAN SUIT' />
                        <p> 1 o 2 Habitacion/s Doble/s</p>
                        <p>Incluye Jacuzzi</p>
                    </div>

                    <div className='d-flex flex-column'>
                        <img src='https://maleconinn.com/wp-content/uploads/2021/03/jacuzzi-a.jpg' width="200" height="200" alt='cuarto' />
                        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlJ_u9n6VplSDsVWGQJdCRb_m6qsas7e658A&usqp=CAU' width="200" height="200" className='mt-4' alt='cuarto' />
                    </div>
                    <div className='m-3 d-flex flex-column'>
                        <input type='text' placeholder='DOBLE' />
                        <p>1 Habitacion Doble</p>
                        <p>1 Baño</p>
                        <input type='text'
                            placeholder='JUNIOR' />
                        <p>1 Habitacion Simple</p>
                        <p>1 Baño</p>
                    </div>
                </div>
                <div className='mt-3'>
                    <p>
                        En caso de daños se cobrara el valor del mismo
                    </p>
                </div>
                <div className="section colm colm6">
                    <label className="switch">
                        <input type="checkbox" name="switch1" id="switch1" value="switch1" />
                        <span className="switch-label" data-on="YES" data-off="NO"></span>
                        <span> Aceptar terminos y condiciones</span>
                    </label>
                </div>
                <div>
                        <button type="submit" className="m-4 btn btn-primary">Reservar</button>
                    </div>
            </div>
        </div>
    );
};

export default ReservarHabitacionView;