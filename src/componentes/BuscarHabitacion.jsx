import React from "react";

import SelectorFecha from "../componentes/SelectorFecha";
import img2 from "../images/img2.jpeg";
import { useEffect, useState } from 'react';
import { Habitaciones } from "../hooks/ConexionSw";
import swal from 'sweetalert';

const BuscarHabitacion = () => {

  const [info, setInfo] = useState(undefined);
  const [llamada, setllamada] = useState(false);

  const mensaje = (texto) => swal(
    {
      title: "Error",
      text: texto,
      icon: "error",
      button: "Aceptar",
      timer: 2000
    }
  );

  if (!llamada) {
    const datos = Habitaciones().then((data) => {
      //console.log(data.fila);
      setllamada(true);
      setInfo(data);
      //console.log(data);
    }, (error) => {
      //console.log(error);
      mensaje(error.mensaje);
    });
  }
  return (

    <div style={{ height: "1460px", width: "2600px" }}>

      


      <br></br>
      <br></br>

      <h2 className="container   "><b>Habitaciones</b></h2>

      <div className="container mt-5  ">
        <table className="table table-hover table-dark tableFixHead ">
          <thead className="border-light">
            <tr>
            <th scope="col">
                <strong> Numero</strong>
              </th>
              <th scope="col">
                <strong> NumeroHabitacion</strong>
              </th>
              <th scope="col">
                <strong> Estado de Habitacion</strong>
              </th>
              <th scope="col">
                <strong> Tipo de Habitacion </strong>
              </th>
              <th scope="col">
                <strong> precio </strong>
              </th>
              <th scope="col">
                <strong> Imagen </strong>
              </th>
            </tr>
          </thead>
          <tbody>
            {info && info.data && info.data.map((element, key) => {
              var estado = "";
              if (element.estadoHabitacion = true) {
                estado = "DISPONIBLE"
              }else{
                estado = "RESERVADO"
              }
              return <tr key={key}>
                <td>{(key) + 1}</td>
                <td> {element.numeroHabitacion}</td>
                <td> {estado}</td>
                <td>{element.tipoHabitacion}</td>
                <td>{element.precioHabitacion}</td>
                <td> <img src={element.imagen} /></td>
              </tr>
            })}
          </tbody>
        </table>
      </div>
      <dir></dir>

    </div>
  );
};

export default BuscarHabitacion;

