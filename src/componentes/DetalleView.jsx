import React from "react";
import { useForm } from "react-hook-form";
import swal from "sweetalert";
import {
  Servicios,
  Habitaciones,
  Detalles,
  DetallesGuardar,
} from "../hooks/ConexionSw";
import { useNavigate, Navigate, Link,useParams } from "react-router-dom";
import { useState } from "react";

import Modal from "react-modal";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    height: "100%",
    bottom: "0%",
    marginRight: "auto",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#5E5E5E",
  },
};
const mensaje = (texto) =>
  swal({
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 2000,
  });

const mensajeOk = (texto) =>
  swal({
    title: "Ingresado Correctamente",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 2000,
  });

const DetalleView = () => {
  //const { register, handleSubmit, formState: { errors } } = useForm();
  const { external } = useParams();
  var exHabitacion;
  var exServicio;
  const [modalIsOpenRegistro, setIsOpen] = React.useState(false);
  const [modalIsOpenHabitacion, setIsOpenHabitacion] = React.useState(false);
  const [modalIsOpenServico, setIsOpenServicio] = React.useState(false);

  const [count, setCount] = useState("");
  const [count2, setCount2] = useState("");
  exHabitacion = { count }.count;
  exServicio = { count2 }.count2;
  console.log("", count.toString);
  console.log("", count2.toString);
  console.log("ExtenalHabitacion:", exHabitacion);
  console.log("ExtenalServicio:", exServicio);

  function openModalRegitro() {
    setIsOpen(true);
  }

  function closeModalRegistro() {
    setIsOpen(false);
  }

  function openModalHabitacion() {
    setIsOpenHabitacion(true);
  }

  function closeModalHabitacion() {
    setIsOpenHabitacion(false);
  }

  function openModalServico() {
    setIsOpenServicio(true);
  }

  function closeModalServicio() {
    setIsOpenServicio(false);
  }

  const [info, setInfo] = useState(undefined);
  const [llamada, setllamada] = useState(false);
  //const { register, handleSubmit, formState: { errors } } = useForm();

  if (!llamada) {
    const datos = Habitaciones().then(
      (data) => {
        //console.log(data.fila);
        setllamada(true);
        setInfo(data);
        console.log("Datas es:", data);
      },
      (error) => {
        console.log("El erroe es:", error);
        mensaje(error.mensaje);
      }
    );
  }
  ////////////Sservico//////////////
  const [infoServ, setinfoServ] = useState(undefined);
  const [llamadaServ, setllamadaServ] = useState(false);
  //const { register, handleSubmit, formState: { errors } } = useForm();

  if (!llamada) {
    const datos = Servicios().then(
      (data) => {
        //console.log(data.fila);
        setllamadaServ(true);
        setinfoServ(data);
        console.log("Datas es:", data);
      },
      (error) => {
        console.log("El erroe es:", error);
        mensaje(error.mensaje);
      }
    );
  }
  ///////////////////////
  const [infoDetalle, setInfoDetalle] = useState(undefined);
  const [llamadaDetalle, setllamadaDetalle] = useState(false);
  //const { register, handleSubmit, formState: { errors } } = useForm();

  if (!llamadaDetalle) {
    const datos = Detalles().then(
      (data) => {
        //console.log(data.fila);
        setllamadaDetalle(true);
        setInfoDetalle(data);
        console.log("Datas es detalle:", data);
      },
      (error) => {
        console.log("El erroe es:", error);
        mensaje(error.mensaje);
      }
    );
  }
  ///=========================
  const [infoDetalleGuaradar, setDetalleGuaradar] = useState(undefined);
  const [llamadaDetalleGuaradar, setLlamadaDetalleGuaradar] = useState(false);

  const navegation = useNavigate();
  //mensaje de error

  //VALIDAR DATOS
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    var datos = {
      //lo que esta en la data
      cantidad: data.cantidad,
    };

    console.log("datos registro", data);
    const registros = DetallesGuardar(datos, exHabitacion, exServicio).then(
      (info) => {
        console.log(info);
        if (info) {
          //sesion token(info.token);
          mensajeOk("se ha registrado");
          navegation("/Detalle");
        }
      },
      (error) => {
        mensaje(error.message);
      }
    );
  };

  var cantidadDeClaves = Object.keys(errors).length;

  /////
  return (
    <div className="container">
      <div onSubmit={handleSubmit(onSubmit)} className="container">
        <div col-12>
          <div style={{ margin: "30px" }}>
            <div className="">
              <button
                style={{ backgroundColor: "#018f8f" }}
                onClick={openModalRegitro}
                type="submit"
                className="btn btn-primary"
              >
                Registrar
              </button>
            </div>
          </div>
        </div>
        <div className="col-12">
          <Modal
            isOpen={modalIsOpenRegistro}
            onRequestClose={closeModalRegistro}
            style={customStyles}
            contentLabel="Example Modal"
          >
            <div style={{ float: "right", margin: "-10px" }}>
              <svg
                onClick={closeModalRegistro}
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                fill="currentColor"
                class="bi bi-x-circle"
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </div>
            <div className="col-12">
              <div>
                <div>
                  <center>
                    <h3>Registrar</h3>
                  </center>
                </div>
                <div>
                  <div class="btn-group btn-group-lg">
                    <div className="col-md-8">
                      <button
                        style={{ backgroundColor: "#018f8f" }}
                        onClick={openModalHabitacion}
                        type="submit"
                        className="btn btn-primary"
                      >
                        Buscar habitacion
                      </button>
                    </div>

                    <div className="col-md-8">
                      <button
                        style={{ backgroundColor: "#018f8f" }}
                        onClick={openModalServico}
                        type="submit"
                        className="btn btn-primary"
                      >
                        Buscar Servicio
                      </button>
                    </div>
                  </div>

                  <div class="col-9">
                    <h5>
                      <b>Datos de la habitacion</b>
                    </h5>
                  </div>

                  <div className="container-fluid h-200">
                    <div class="col-12">
                      <div className="col v-center">
                        <div className="row">
                          <div class="col-12">
                            <div class="p-3 border bg-light">
                              <div class="col-11">
                                <p>Numero: 11</p>
                                <p>Tipo: Matrimonial</p>
                                <p>Estado: Alquilada</p>
                                <p>Descripcion: Habitacion de pareja</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div>
                  <div class="col-9">
                    <h5>
                      <b>Datos de Servicio</b>
                    </h5>
                  </div>

                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="col-3">
                      <label for="Exampleusuario" class="form-label">
                        Numeros de servicio
                      </label>
                      <input
                        id="cantidad"
                        {...register("cantidad", { required: true })}
                        required
                        class="form-contrl"
                        type="number"
                        placeholder="Cantidades"
                      />
                    </div>
                  </form>
                  <div className="col-9">
                    <div className="container-fluid h-100">
                      <div class="col-12">
                        <div className="col v-center">
                          <div className="row">
                            <div class="col-12">
                              <div class="p-3 border bg-light">
                                <div class="col-11">
                                  <p>Nombre: comida</p>
                                  <p>Precio: 26.60</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="container-fluid h-100">
                    <div className="row w-100">
                      <div className="col v-center">
                        <div
                          className="btn d-block mx-auto"
                          text-align="center"
                        >
                          <button
                            id="buttonGuardar"
                            type="submit"
                            className="btn btn-success"
                          >
                            Registrar Detalle
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <Modal
              isOpen={modalIsOpenHabitacion}
              onRequestClose={closeModalHabitacion}
              style={customStyles}
              contentLabel="Example Modal"
            >
              <div style={{ float: "right", margin: "-10px" }}>
                <svg
                  onClick={closeModalHabitacion}
                  xmlns="http://www.w3.org/2000/svg"
                  width="40"
                  height="40"
                  fill="currentColor"
                  class="bi bi-x-circle"
                  viewBox="0 0 16 16"
                >
                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
              </div>
              <div lassName="col-12">
                <div c>
                  <h1
                    className="text-center"
                    style={{ color: "#212529", fontWeight: "bold" }}
                  >
                    LISTA DE HABITACION
                  </h1>
                </div>

                <div className="col-12  ">
                  <table className="table table-hover table-dark tableFixHead ">
                    <thead className="border-light">
                      <tr>
                        <th>#</th>
                        <th>Habitacion</th>
                        <th>Tipo</th>
                        <th>Descripcion </th>
                        <th>Precio</th>
                        <th>Estado</th>
                        <th>Seleccionar</th>
                      </tr>
                    </thead>
                    <tbody>
                      {info &&
                        info.data &&
                        info.data.map((element, key) => {
                          var estado;
                          if (element.estadoHabitacion == true) {
                            estado = "Disponible";
                          } else {
                            estado = "Ocupada";
                          }

                          return (
                            <tr key={key}>
                              <td>{key + 1}</td>
                              <td>{element.numeroHabitacion} </td>
                              <td>{element.tipoHabitacion} </td>
                              <td>{element.descripcion} </td>
                              <td>{element.precioHabitacion} </td>
                              <td>{estado}</td>
                              <td>
                                <button
                                  style={{ backgroundColor: "#018f8f" }}
                                  onClick={() => setCount(element.external)}
                                  className="btn btn-primary"
                                >
                                  Seleccionar
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </Modal>
            <Modal
              isOpen={modalIsOpenServico}
              onRequestClose={closeModalServicio}
              style={customStyles}
              contentLabel="Example Modal"
            >
              <div style={{ float: "right", margin: "-10px" }}>
                <svg
                  onClick={closeModalHabitacion}
                  xmlns="http://www.w3.org/2000/svg"
                  width="40"
                  height="40"
                  fill="currentColor"
                  class="bi bi-x-circle"
                  viewBox="0 0 16 16"
                >
                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
              </div>
              <div>
                <div className="container">
                  <h1
                    className="text-center"
                    style={{ color: "#212529", fontWeight: "bold" }}
                  >
                    LISTA DE SERVICOS
                  </h1>
                </div>

                <div className="col-12  ">
                  <table className="table table-hover table-dark tableFixHead ">
                    <thead className="border-light">
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Seleccionar</th>
                      </tr>
                    </thead>
                    <tbody>
                      {infoServ &&
                        infoServ.data &&
                        infoServ.data.map((serv, key) => {
                          return (
                            <tr key={key}>
                              <td>{key + 1}</td>
                              <td>{serv.Nombre_Servicio} </td>
                              <td>{serv.precio} </td>
                              <td>
                                <button
                                  style={{ backgroundColor: "#018f8f" }}
                                  onClick={() => setCount2(serv.external)}
                                  className="btn btn-primary"
                                >
                                  Seleccionar
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </Modal>
          </Modal>
        </div>

        <div>
          <div className="container">
            <h1
              className="text-center"
              style={{ color: "#212529", fontWeight: "bold" }}
            >
              DETALLES
            </h1>
          </div>

          <div className="container">
            <table className="table table-hover table-dark tableFixHead ">
              <thead className="border-light">
                <tr>
                  <th>#</th>
                  <th>Descripcion</th>
                  <th>Servicio</th>
                  <th>Precio</th>
                  <th>Cantida</th>
                  <th>Total</th>
                  <th>Habitacion</th>
                  <th>Tipo </th>
                </tr>
              </thead>
              <tbody>
                {infoDetalle &&
                  infoDetalle.data &&
                  infoDetalle.data.map((element, key) => {
                    return (
                      <tr key={key}>
                        <td>{key + 1}</td>
                        <td>{element.descripcion} </td>
                        <td>{element.servicios} </td>
                        <td>{element.precioUnitario} </td>
                        <td>{element.cantidad} </td>
                        <td>{element.precioTotal} </td>
                        <td>{element.N_habitacion} </td>
                        <td>{element.habitacion} </td>
                        <td> <Link to={"/VisualizarReservaciones/"+external+"/"+element.external} className='btn btn-dark'  onClick={<Navigate to={"/VisualizarReservaciones/"+external+"/"+element.external}/>}> Seleccionar </Link> </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetalleView;
