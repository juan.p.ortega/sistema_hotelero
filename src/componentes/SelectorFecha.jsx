import React from 'react';
import { useState } from 'react';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const SelectorFecha = () => {
    /* const [startDate, setStartDate] = useState(new Date());
    return (
        <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} id="botonBuscarHabitacion2"
        placeholder="&#xf133;  Fecha de ingreso"/>
      );
}; */
const [startDate, setStartDate] = useState(new Date());
  return (
    <ReactDatePicker
      selected={startDate}
      onChange={(date) => setStartDate(date)}
      className="red-border" id="botonBuscarHabitacion2"
    />
  );
};
export default SelectorFecha;
