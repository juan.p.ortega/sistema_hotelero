import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

const Nanvar = () => {
    return (
        <div style={{ backgroundColor: "#D2BC97", borderStyle: "solid"}}>
                <nav className="navbar navbar-expand-lg navbar-light">
                    <div className="container-fluid">
                        <button className="navbar-toggler" type="button" data-mdb-toggle="collapse"
                            data-mdb-target="#navbarExample01" aria-controls="navbarExample01" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarExample01">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item active">
                                    <Link className="nav-link" to="/Inicio"> <h4>Inicio </h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/IngresarServicios/:external"><h4>Servicios</h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/VisualizarReservaciones/:external/:external2"><h4>Reservaciones</h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/Habitaciones"><h4>Habitaciones</h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/Cliente"><h4>Clientes</h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/Factura"><h4>Factura</h4></Link>
                                </li>
                                <li className="nav-item mx-3">
                                    <Link className="nav-link" to="/Detalle/:external"><h4>Detalle</h4></Link>
                                </li>
                                <div className='ml-3'>
                                     <Link className="btn btn-dark" to="/">  <p className='justify-content-center' style={{color: "#FFFFFF"}}> <b>Cerrar Sesion </b> </p>  </Link>
                                 </div>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    );
};

export default Nanvar;