import React from 'react';
import '../css/fondoInicio.css';

const PaginaInicio = () => {
    return (
        <header>
            <div className="p-5 text-center bg-image " style={{ backgroundImage: "url('https://cache.marriott.com/marriottassets/marriott/SAOBR/saobr-lounge-bar-2345-hor-wide.jpg?interpolation=progressive-bilinear&downsize=2600px:*')", height: "1460px" ,width: "2400px" }}>
                <div className="mask" style={{ backgroundColor: "rgba(0, 0, 0, 0.6)" }}>
                    <div className="d-flex justify-content-center align-items-center h-100">
                        <div className="text-white">
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default PaginaInicio;