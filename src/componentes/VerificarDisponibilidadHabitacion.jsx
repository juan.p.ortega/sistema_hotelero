import React from "react";
import img3VH from "../images/img3VH.jpg";
import SelectorFecha from "./SelectorFecha";
import imgc1 from "../images/imgc1.jpg";
import imgc2 from "../images/imgc2.jpg";
const VerificarDisponibilidadHabitacion = () => {
  return (
    <div className="grupo-Buscar-Habitacion1" style={{height: "1460px" ,width: "2600px"}}>
      <div className="card" style={{ margin: "0px auto", width: "40rem" }}>
        <img
          src={img3VH}
          className="card-img-top"
          alt="imagenPrincipal"
          /* style={{ height: "10rem" }} */
        />
        <div className="card-body">
          <div id="group3enlinea">
            <div className="grupo-Buscar-Habitacion">
              {/* <input
            id="botonBuscarHabitacion2"
            placeholder="&#xf133;  Fecha de ingreso"
          />   */}
              <SelectorFecha />
            </div>

            <div className="grupo-Buscar-Habitacion">
              {/* <input
            type="text"
            id="botonBuscarHabitacion2"
            placeholder="&#xf133;  Fecha de salida"
          />  */}
              <SelectorFecha />
            </div>
            <button className="btn btn-primary">BUSCAR</button>
          </div>
        </div>
      </div>
      <br></br>
      <h3>Disponibilidad:</h3>
      <div className="card" style={{ margin: "0px auto", width: "40rem" }}>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Tipo</th>
              <th scope="col">Capacidad</th>
              <th scope="col">Valor</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">
                <div
                  className="card"
                  style={{ margin: "0px auto", width: "20rem" }}
                >
                  <img
                    src={imgc1}
                    className="card-img-top"
                    style={{ height: "10rem" }}
                    alt="imagenCuarto1"
                  />
                </div>
              </th>
              <td>2 personas</td>
              <td>$120,00</td>
              <td>
                <button type="button" class="btn btn-danger">
                  Reservar
                </button>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <div
                  className="card"
                  style={{ margin: "0px auto", width: "20rem" }}
                >
                  <img
                    src={imgc2}
                    className="card-img-top"
                    style={{ height: "10rem" }}
                    alt="imagenCuarto2"
                  />
                </div>
              </th>
              <td>2 personas</td>
              <td>$130,00</td>
              <td>
                <button type="button" class="btn btn-danger">
                  Reservar
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default VerificarDisponibilidadHabitacion;
