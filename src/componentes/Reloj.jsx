import React, { useEffect, useState } from 'react';
//import '../css/d.css';

export const Reloj = () => {

    const date = new Date();

    const [dataTime, setDataTime] = useState({
        horas: date.getHours(),
        minutos: date.getMinutes(),
        segundos: date.getSeconds()
    });


    useEffect(() => {
        
        const timer = setInterval(() => {
            
            const date = new Date();
            setDataTime({
                horas: date.getHours(),
                minutos: date.getMinutes(),
                segundos: date.getSeconds(),
            });
            
        }, 1000);
        

        return () => clearInterval(timer);

    }, []);

    /*style={{position: "absolute" ,top:"38%",left:"93%", transform: "translate(-50%, -50%)", color:"#000000"}}*/
    return (
                <div className='mx-4 ' style={{float: "right", color:"#000000"}}>
                    <h4>
                        <b>{dataTime.horas < 10 ? ` 0${dataTime.horas}` : dataTime.horas}:{dataTime.minutos < 10 ? ` 0${dataTime.minutos}` : dataTime.minutos}:{dataTime.segundos < 10 ? ` 0${dataTime.segundos}` : dataTime.segundos} </b>
                    </h4>
                </div>
    )
}

export default Reloj;