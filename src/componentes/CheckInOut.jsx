import "../css/Bootstrap.css";
import React from "react";
import Modal from "react-modal";

import DatePicker from "react-datepicker";
import { useState } from "react";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    height: "100%",
    bottom: "0%",
    marginRight: "auto",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#5E5E5E",
  },
};

const CheckInOut = () => {
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  const [modalIsOpenCosto, setIsOpenCosto] = React.useState(false);

  function openModalCostos() {
    setIsOpenCosto(true);
  }

  function closeModalCostos() {
    setIsOpenCosto(false);
  }

  return (
    <div>
      <div className="container">
        <div className="col-6">
          <form class="d-flex">
            <input
              class="form-control me-2"
              type="search"
              placeholder="Buscar cliente"
              aria-label="Search"
            />
            <button class="btn btn-lg btn-outline-primary" type="submit">
              Buscar
            </button>
          </form>
        </div>
        <div className="col-5 ">
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              id="checkDisponible"
            />
            <label className="form-check-label" for="checkDisponible">
              Disponibles
            </label>
          </div>

          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              id="checkOcupada"
            />
            <label className="form-check-label" for="checkOcupada">
              Ocupadas
            </label>
          </div>

          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              id="checkMantenimiento"
            />
            <label className="form-check-label" for="checkMantenimiento">
              Mantenimiento
            </label>
          </div>
        </div>
        <div className="col-12  ">
          <table className="table table-hover table-dark tableFixHead ">
            <thead className="border-light">
              <tr>
                <th>Nro Habitacion</th>
                <th>Tipo</th>
                <th>Estado</th>
                <th>Descripcion </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Matrimonial</td>
                <td>Disponible</td>
                <td>Pagado realizado</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Doble</td>
                <td>Ocupada</td>
                <td>Pendiente pago</td>
              </tr>
            </tbody>
          </table>
          <div className="col-12">
            <nav aria-label="Page navigation example">
              <ul className="pagination">
                <li className="page-item">
                  <a className="page-link" href="#">
                    Previous
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    Next
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div style={{ margin: "30px" }}>
          <div>
            <button
              style={{ backgroundColor: "#018f8f" }}
              onClick={openModal}
              type="submit"
              className="btn btn-primary"
            >
              Check In
            </button>
          </div>
        </div>

        <div style={{ margin: "30px" }}>
          <div>
            <button
              style={{ backgroundColor: "#018f8f" }}
              onClick={openModalCostos}
              type="submit"
              className="btn btn-primary"
            >
              Costos Adicionales
            </button>
          </div>
        </div>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <div style={{ float: "right", margin: "-10px" }}>
          <svg
            onClick={closeModal}
            xmlns="http://www.w3.org/2000/svg"
            width="40"
            height="40"
            fill="currentColor"
            class="bi bi-x-circle"
            viewBox="0 0 16 16"
          >
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </div>
        <div>
          <div>
            <div>
              <center>
                <h3>Check In</h3>
              </center>
            </div>
            <div>
              <h5>
                <b>Datos de la habitacion</b>
              </h5>
            </div>

            <div>
              <div className="container-fluid h-100">
                <div className="row w-100">
                  <div className="col v-center">
                    <div className="row">
                      <div className="col-3">
                        <img
                          src="https://cdn-icons-png.flaticon.com/512/531/531234.png"
                          width="50%"
                          alt=""
                        />
                      </div>
                      <div class="col-6">
                        <div class="p-3 border bg-light">
                          <div></div>
                          <div>
                            <p>Numero: </p>
                            <p>Tipo: </p>
                            <p>Estado: </p>
                            <p>Descripcion: </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <form className="row g-3">
              <div className="col-6">
                <h5>
                  <b>Datos clientes</b>
                </h5>
              </div>
              <div className="col-6">
                <h5>
                  <b>Datos de alojamiento</b>
                </h5>
              </div>

              <div className="col-6">
                <div>
                  <label for="inputAddress" className="form-label">
                    Cedula
                  </label>
                  <div>
                    <input
                      type="text"
                      className="form-control"
                      id="inputCedula"
                      placeholder="Ingrese numero de cedula"
                    />
                  </div>
                </div>
              </div>

              <div className="col-3">
                <label for="inputAddress" className="form-label">
                  Precio
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputPrecio"
                  placeholder="Precio de habitacion"
                />
              </div>

              <div className="col-3">
                <label for="inputAddress" className="form-label">
                  Adelanto
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputAdelanto"
                  placeholder="Cantida Pago"
                />
              </div>
              <div className="col-6">
                <label for="inputAddress" className="form-label">
                  Nombres
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputNombre"
                  placeholder="Nombres del cliente"
                />
              </div>

              <div className="col-6">
                <label for="inputAddress" className="form-label">
                  Fecha de ingreso
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputFechaEntrada"
                  placeholder="Fecha de ingreso"
                />
              </div>
              <div className="col-md-6">
                <label for="inputEmail" className="form-label">
                  Email
                </label>
                <input type="email" className="form-control" id="inputEmail" />
              </div>
              <div className="col-6">
                <label for="inputAddress" className="form-label">
                  Fecha de Salida
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputFechaSalida"
                  placeholder="Fecha de salida"
                />
              </div>

              <div className="col-6">
                <label for="inputAddress" className="form-label">
                  Telefono
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="inputTeefono"
                  placeholder="Telefono/Celular"
                />
              </div>
            </form>
            <div className="container-fluid h-100">
              <div className="row w-100">
                <div className="col v-center">
                  <div className="btn d-block mx-auto" text-align="center">
                    <button type="submit" className="btn btn-primary">
                      Registrar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>

      <Modal
        isOpen={modalIsOpenCosto}
        onRequestClose={closeModalCostos}
        style={customStyles}
        contentLabel="Example"
      >
        <div style={{ float: "right", margin: "-10px" }}>
          <svg
            onClick={closeModalCostos}
            xmlns="http://www.w3.org/2000/svg"
            width="40"
            height="40"
            fill="currentColor"
            class="bi bi-x-circle"
            viewBox="0 0 16 16"
          >
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </div>

        <div>
          <center>
            <h5>
              <b>Costos Adicionales</b>
            </h5>
          </center>
        </div>
        <form className="row g-3">
          <div className="col-6">
            <div>
              <b>Habitacion</b>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="p-3 border bg-light">
                  <div>
                    <p>Numero:</p>
                    <p>Tipo:</p>
                    <p>Estado:</p>
                    <p>Descripcion</p>
                    <p></p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-6">
            <div>
              <b>Cliente</b>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="p-3 border bg-light">
                  <div>
                    <p>Cedula:</p>
                    <p>Nombre:</p>
                    <p>Direccion:</p>
                    <p>Correo:</p>
                    <p>Fecha Entrada:</p>
                    <p>Fecha Salida:</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12">
            <h1> </h1>
          </div>

          <div className="col-12">
            <div className="text-white bg-primary mb-3">
              <center>
                <h5>
                  <b>Costos de Alojamiento</b>
                </h5>
              </center>
            </div>
            <div>
              <table className="table table-hover table-dark tableFixHead ">
                <thead className="border-light">
                  <tr>
                    <th>Costo calculado</th>
                    <th>Dinero Pagado</th>
                    <th>Pago restante</th>
                  </tr>
                </thead>
                <tbody>
                  <td>$ 240</td>
                  <td>$ 150</td>
                  <td>$ 90</td>
                </tbody>
              </table>
            </div>
          </div>

          <div className="col-12">
            <div className="text-white bg-primary mb-3">
              <center>
                <h5>
                  <b>Servicios</b>
                </h5>
              </center>
            </div>

            <table className="table table-hover table-dark tableFixHead ">
              <thead className="border-light">
                <tr>
                  <th>Cantidad</th>
                  <th>Descripcion</th>
                  <th>Precio Unitario</th>
                  <th>Estado</th>
                  <th> </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Galletas Marinas</td>
                  <td>$ 1.50</td>
                  <td>Pagado</td>
                  <td>$ 0.00</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Chocolates</td>
                  <td>$ 3.00</td>
                  <td>Pendiente</td>
                  <td>$ 3.00</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-9 card text-white bg-primary mb-3 ">
            <div className="text-center ">
              <center>
                <h5>
                  <b>Total a pagar</b>
                </h5>
              </center>
            </div>
          </div>
          <div className="col-3 card text-black bg-light mb-3">
            <div className="card-body text-center">$ 93.00</div>
          </div>

          <div className="col-12">
            <div className="container-fluid h-100">
              <div className="row w-100">
                <div className="col v-center">
                  <div className="btn d-block mx-auto" text-align="center">
                    <button type="submit" className="btn btn-success">
                      Terminar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default CheckInOut;


