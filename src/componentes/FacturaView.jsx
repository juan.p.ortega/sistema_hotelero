import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import swal from 'sweetalert';
import { Reservacionesf, Servicios } from '../hooks/ConexionSw';
import { Router, Link, Navigate } from 'react-router-dom';

const mensaje = (texto) => swal(
    {
        title: "Error",
        text: texto,
        icon: "error",
        button: "Aceptar",
        timer: 2000
    }
);

const mensajeOk = (texto) => swal(
    {
        title: "Ingresado Correctamente",
        text: texto,
        icon: "success",
        button: "Aceptar",
        timer: 2000
    }
);


const FacturaView = () => {
    const [info, setInfo] = useState(undefined);
    const [llamada, setllamada] = useState(false);
    //const { register, handleSubmit, formState: { errors } } = useForm();

    if (!llamada) {
        const datos = Reservacionesf().then((data) => {
            //console.log(data.fila);
            setllamada(true);
            setInfo(data);
            console.log("Datas es:", data);
        }, (error) => {
            console.log("El erroe es:", error);
            mensaje(error.mensaje);
        });
    }


    return (

        <div>

            <div className='container'>
                <h1 className='text-center' style={{ color: "#212529", fontWeight: 'bold' }}>LISTA DE FACTURAS</h1>
            </div>

            <div className="col-12  ">
                <table className="table table-hover table-dark tableFixHead ">
                    <thead className="border-light">
                        <tr>
                            <th>Nro</th>
                            <th>Fecha</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>Cliente</th>
                            <th>Servicio</th>
                            <th>Catidad</th>


                            <th>Precio total </th>
                            <th>Nro Habitacion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {info && info.data && info.data.map((element, key) => {
                            return <tr key={key}>
                                <td>{(key) + 1}</td>
                                <td>{element.Creacion} </td>

                                <td>{element.Fecha_Entrada} </td>
                                <td>{element.Fecha_Salida} </td>
                                <td>{element.CLiente} </td>
                                <td>{element.Detalles_Nombre_Servicio} </td>
                                <td>{element.Cantidad} </td>
                                <td>{element.Precio_Total} </td>
                                <td>{element.Detalles_NHabitacion} </td>
                            </tr>
                        })}

                    </tbody>
                </table>
            </div>
        </div>

    );
};

export default FacturaView;