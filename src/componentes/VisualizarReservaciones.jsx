import React from 'react';
import '../css/Bootstrap.css';
import swal from 'sweetalert';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import SelectorFecha from "./SelectorFecha";
import { IngresarReservaciones, Reservaciones } from '../hooks/ConexionSw';
import { Link, useNavigate, useParams } from 'react-router-dom';


const VisualizarReservaciones = () => {

    const { external } = useParams();
    const { external2 } = useParams();

    const [info, setInfo] = useState(undefined);
    const [llamada, setllamada] = useState(false);
    const { register, handleSubmit, formState: { errors } } = useForm();

    const [d, setD] = useState(0);
    const [e, setE] = useState(0);
    var exPersona;
    var exDetalle;

    const [count, setCount] = useState("");
    const [count2, setCount2] = useState("");
    //exPersona = { count };
    //exDetalle = { count2 };

    const mensaje = (texto) => swal(
        {
            title: "Error",
            text: texto,
            icon: "error",
            button: "Aceptar",
            timer: 2000
        }
    );

    const mensajeOk = (texto) => swal(
        {
            title: "Ingresado Correctamente",
            text: texto,
            icon: "success",
            button: "Aceptar",
            timer: 2000
        }
    );

    function handleButtonClick() {
        exPersona = external;
        console.log(exPersona);
        setD(1);
    }
    function handleButtonClick1() {
        exDetalle = external;
        console.log(exDetalle);
        setE(1);
    }

    if (!llamada) {
        const datos = Reservaciones().then((data) => {
            //console.log(data.fila);
            setllamada(true);
            setInfo(data);
            console.log(data);

            console.log(exPersona);
            console.log(exDetalle);

        }, (error) => {
            //console.log(error);
            mensaje(error.mensaje);
        });
    }

    const onSubmit = (data) => {
        console.log("---//"+external);
        console.log("****"+external2);
        var datos = { 'fecha': data.fecha, 'fecha_entrada': data.fecha_entrada, 'fecha_salida': data.fecha_salida, 'costo_total': 0, 'estadoFactura': 0 };
        const val = IngresarReservaciones(external, external2, datos).then((info) => {
            if (info) {
                console.log("---", info.data);
                //console.log(info.datos);
                mensajeOk("Se han ingresado los datos");
            } else {
                mensaje(info.message);
                console.log("NO Se han ingresado los datos");
            }
        });
    };


    return (
        <div className='container' style={{ height: "1460px", width: "2600px" }}>
            <div className='row d-flex justify-content-center'>
                <h1 className='font-weight-bold  '> VISUALIZAR Y REGISTRAR RESERVACIONES </h1>
                <img className='mx-4' src="https://cdn-icons-png.flaticon.com/512/2636/2636399.png" width={"70"} height={"85"} ></img>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row d-flex justify-content-center">
                    <div>
                        <button onClick={handleButtonClick} className="nav-item mx-3 btn btn-dark">
                            <Link className="btn btn-dark" to="/Cliente"><h5><b>Cliente</b></h5></Link>
                        </button>
                    </div>

                    <div className='mx-4'>
                        <button onClick={handleButtonClick1} className="nav-item mx-3 btn btn-dark">
                            <Link className="btn btn-dark" to={`/Detalle/${external}`}><h5><b>Detalles</b></h5></Link>
                        </button>
                    </div>

                    <div className="mx-3">
                        <label className='mx-3' > <b> Fecha </b> </label>
                        <input type="date" id="start" name="trip-start"
                            min="2023-02-24" max="2030-12-31"  step="any"{...register('fecha', { required: true })}/>
                            {errors.fecha && errors.fecha.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere la fecha</div>}
                    </div>
                    <div className="mx-3">
                        <label className='mx-3' > <b> Fecha Entrada</b> </label>
                        <input type="date" id="start" name="trip-start"
                            min="2023-02-24" max="2030-12-31" step="any"{...register('fecha_entrada', { required: true })}/>
                            {errors.fecha_entrada && errors.fecha_entrada.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere la fecha_entrada</div>}
                    </div>
                    <div className="mx-3 mt-5">
                        <label className='mx-3' > <b> Fecha Salida </b> </label>
                        <input type="date" id="start" name="trip-start"
                            min="2023-02-24" max="2030-12-31"  step="any"{...register('fecha_salida', { required: true })}/>
                            {errors.fecha_salida && errors.fecha_salida.type === 'required' && <div className='alert alert-danger fade show' role='alert'>Se requiere la fecha_salida</div>}
                    </div>

                    <div className="mx-5 mt-5">
                        <button type="submit" className="btn btn-dark"> <b>Ingresar Reservacion</b></button>
                    </div>
                </div>
            </form>


            <div className="container mt-5  ">
                <table className="table table-hover table-dark tableFixHead " >
                    <thead className="border-light" >
                        <tr>
                            <th scope="col"><strong> Numero</strong></th>
                            <th scope="col"><strong> CLiente</strong></th>
                            <th scope="col"><strong> Nombre Servicio</strong></th>
                            <th scope="col"><strong> fecha_entrada, '' Servicio</strong></th>
                            <th scope="col"><strong> Numero de Habitacion </strong></th>
                            <th scope="col"><strong> Precio Habitacion </strong></th>
                            <th scope="col"><strong> Cantidad </strong></th>
                            <th scope="col"><strong> Costo Total </strong></th>
                            <th scope="col"><strong> Fecha Salida </strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        {info && info.data && info.data.map((element, key) => {
                            return <tr key={key}>
                                <td>{(key) + 1}</td>
                                <td> {element.CLiente}</td>
                                <td> {element.Detalles_Nombre_Servicio}</td>
                                <td>{element.PrecioServicio}</td>
                                <td>{element.Detalles_NHabitacion}</td>
                                <td>{element.PrecioHabitacion}</td>
                                <td>{element.Cantidad}</td>
                                <td>{element.Precio_Total}</td>
                                <td>{element.Fecha_Salida}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
            <dir>
                <br />
                <br />
                <br />
                <br />
            </dir>
        </div >
    );
};

export default VisualizarReservaciones;