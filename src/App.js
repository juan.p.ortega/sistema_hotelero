import React from "react";
import CabeceraFija from "./componentes/CabeceraFija";
import './App.css';
//import './css/d.css';
//import '../src/css/fondo.css';
//Componentes
import BuscarHabitacion from "./componentes/BuscarHabitacion";
import VerificarDisponibilidadHabitacion from "./componentes/VerificarDisponibilidadHabitacion";
import PaginaInicio from "./componentes/PaginaInicio";
import CrearCuenta from "./componentes/CrearCuenta";
import IngresarServicios from "./componentes/IngresarServicios";
import VisualizarReservaciones from "./componentes/VisualizarReservaciones";
import PagarView from "./componentes/PagarView";
import RegistrarSistemaView from "./componentes/RegistrarSistemaView";
import ReservarHabitacionView from "./componentes/ReservarHabitacionView";
import { EstaSession } from './utilidades/UseSession';
//NANVAR
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import Nanvar from "./componentes/Nanvar";
import Habitaciones from "./componentes/Habitaciones";
import CheckInOut from "./componentes/CheckInOut";
import ClientesV from "./componentes/ClientesV";
import DetalleView from "./componentes/DetalleView";
import FacturaView from "./componentes/FacturaView";



function App() {
 
  
  const Middleware = ({ children }) => {
    const autenticado = EstaSession();
    const location = useLocation();
    if(autenticado){
      return children;
    } else {
      return <Navigate to="/" state={location} />
    }
  }
  const MiddlewareSession = ({ children }) => {
    const autenticado = EstaSession();
    //const location = useLocation();
    if(autenticado){
      return <Navigate to="/Inicio"/>;
    } else return children;
  }

  return (
    <div>
      <CabeceraFija/>
      <Nanvar/>
 
      <Routes>
        <Route path='/' element = {<MiddlewareSession><RegistrarSistemaView/></MiddlewareSession>}></Route>
        <Route path="CrearCuenta" element = {<MiddlewareSession><CrearCuenta/></MiddlewareSession>}></Route>
        <Route path="/Inicio" element={<Middleware><PaginaInicio/></Middleware>}></Route>
        <Route path="/Cliente" element = {<Middleware><ClientesV/></Middleware>}></Route>
        <Route path='/IngresarServicios/:external' element={<Middleware><IngresarServicios/></Middleware>}> </Route>
        <Route path='/Habitaciones' element={<Middleware><Habitaciones/></Middleware>}></Route>
        <Route path='/BuscarHabitacion' element={<Middleware><BuscarHabitacion/></Middleware>}></Route>
        <Route path='/ReservarHabitacion' element={<Middleware><ReservarHabitacionView/></Middleware>}></Route>
        <Route path='/VisualizarReservaciones/:external/:external2' element={<Middleware><VisualizarReservaciones/></Middleware>}></Route>
        <Route path='/VerificarDisponibilidadHabitacion' element={<Middleware><VerificarDisponibilidadHabitacion/></Middleware>}></Route>
        <Route path='/Detalle/:external' element={<Middleware><DetalleView/></Middleware>}></Route>
        <Route path='/Factura' element={<Middleware><FacturaView/></Middleware>}></Route>
        

      </Routes>
    </div>
  );
}

export default App;

